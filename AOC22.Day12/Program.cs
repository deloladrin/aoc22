using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
      
namespace AOC22.Day12;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static IEnumerable<Point> Neighbors(Point u)
    {
        yield return new Point(u.X, u.Y - 1);
        yield return new Point(u.X + 1, u.Y);
        yield return new Point(u.X, u.Y + 1);
        yield return new Point(u.X - 1, u.Y);
    }
    
    private static Dictionary<Point, Point> Dijkstra(List<char[]> map, Point start)
    {
        Dictionary<Point, int> distances = new Dictionary<Point, int>();
        Dictionary<Point, Point> previous = new Dictionary<Point, Point>();

        List<Point> remaining = new List<Point>();

        for (int y = 0; y < map.Count; y++)
        {
            for (int x = 0; x < map[y].Length; x++)
            {
                Point v = new Point(x, y);
                distances.Add(v, int.MaxValue);
                previous.Add(v, Point.Empty);
                
                remaining.Add(v);
            }
        }

        distances[start] = 0;

        while (remaining.Count > 0)
        {
            Point u = remaining.OrderBy(x => distances[x]).FirstOrDefault();
            remaining.Remove(u);

            foreach (Point v in Neighbors(u))
            {
                if (!remaining.Contains(v))
                    continue;

                if (map[v.Y][v.X] < map[u.Y][u.X] - 1)
                    continue;

                int alternative = distances[u] + 1;

                if (alternative < distances[v])
                {
                    distances[v] = alternative;
                    previous[v] = u;
                }
            }
        }

        return previous;
    }

    private static int Traverse(Dictionary<Point, Point> previous, Point start, Point end)
    {
        Point current = end;
        int count = 0;

        while (current != start)
        {
            current = previous[current];
            count++;
        }

        return count;
    }
    
    private static void Task1(List<char[]> map, Point start, Point end)
    {
        Dictionary<Point, Point> previous = Dijkstra(map, end);
        int total = Traverse(previous, end, start);
        
        Console.WriteLine($"Task1: {total}");
    }
      
    private static void Task2(List<char[]> map, Point end)
    {
        Dictionary<Point, Point> previous = Dijkstra(map, end);
        int minimum = int.MaxValue;

        for (int y = 0; y < map.Count; y++)
            for (int x = 0;x < map[y].Length; x++)
                if (map[y][x] == 'a')
                    minimum = Math.Min(minimum, Traverse(previous, end, new Point(x, y)));

        Console.WriteLine($"Task2: {minimum}");
    }

    private static void Main(string[] args)
    {
        List<char[]> map = new List<char[]>();

        Point start = Point.Empty;
        Point end = Point.Empty;

        foreach (string line in File.ReadAllLines(InputFile))
            map.Add(line.ToCharArray());

        for (int y = 0; y < map.Count; y++)
        {
            for (int x = 0; x < map[y].Length; x++)
            {
                if (map[y][x] == 'S')
                {
                    start = new Point(x, y);
                    map[y][x] = 'a';
                }

                if (map[y][x] == 'E')
                {
                    end = new Point(x, y);
                    map[y][x] = 'z';
                }
            }
        }
        
        Task1(map, start, end);
        Task2(map, end);
    }
}
