using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day08;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static void UpdateDirection(Tree[,] trees, int x, int y, int dx, int dy)
    {
        int max = -1;
        
        while (x >= 0 && x < trees.GetLength(0) &&
               y >= 0 && y < trees.GetLength(1))
        {
            if (trees[x, y].Height > max)
            {
                trees[x, y].Visible = true;
                max = trees[x, y].Height;
            }

            x += dx;
            y += dy;
        }
    }

    private static int VisibleCount(Tree[,] trees)
    {
        int total = 0;
        
        for (int y = 0; y < trees.GetLength(1); y++)
            for (int x = 0; x < trees.GetLength(0); x++)
                if (trees[x, y].Visible)
                    total++;

        return total;
    }
    
    private static void Task1(Tree[,] trees)
    {
        for (int x = 0; x < trees.GetLength(0); x++)
        {
            UpdateDirection(trees, x, 0, 0, 1);
            UpdateDirection(trees, x, trees.GetLength(1) - 1, 0, -1);
        }

        for (int y = 0; y < trees.GetLength(1); y++)
        {
            UpdateDirection(trees, 0, y, 1, 0);
            UpdateDirection(trees, trees.GetLength(0) - 1, y, -1, 0);
        }

        Console.WriteLine($"Task1: {VisibleCount(trees)}");
    }

    private static int ScoreDirection(Tree[,] trees, int x, int y, int dx, int dy)
    {
        int height = trees[x, y].Height;
        x += dx;
        y += dy;

        int score = 0;
        
        while (x >= 0 && x < trees.GetLength(0) &&
               y >= 0 && y < trees.GetLength(1))
        {
            score++;
            
            if (trees[x, y].Height >= height)
                break;

            x += dx;
            y += dy;
        }

        return score;
    }
      
    private static void Task2(Tree[,] trees)
    {
        int max = -1;

        for (int y = 0; y < trees.GetLength(1); y++)
        {
            for (int x = 0; x < trees.GetLength(0); x++)
            {
                int score = ScoreDirection(trees, x, y, -1, 0) *
                            ScoreDirection(trees, x, y, 1, 0) *
                            ScoreDirection(trees, x, y, 0, -1) *
                            ScoreDirection(trees, x, y, 0, 1);

                max = Math.Max(max, score);
            }
        }

        Console.WriteLine($"Task2: {max}");
    }
          
    private static void Main(string[] args)
    {
        string[] lines = File.ReadAllLines(InputFile);
        Tree[,] trees = new Tree[lines[0].Length, lines.Length];

        for (int y = 0; y < trees.GetLength(1); y++)
        {
            for (int x = 0; x < trees.GetLength(0); x++)
            {
                trees[x, y].Height = int.Parse(lines[y][x].ToString());
                trees[x, y].Visible = false;
            }
        }
        
        Task1(trees);
        Task2(trees);
    }
}
