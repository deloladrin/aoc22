namespace AOC22.Day08;

public struct Tree
{
    public int Height;
    public bool Visible;
}