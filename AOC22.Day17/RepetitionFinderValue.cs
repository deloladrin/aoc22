using System;

namespace AOC22.Day17;

public record struct RepetitionFinderValue(int Index, int Height)
{
}