using System;

namespace AOC22.Day17;

public record struct RepetitionFinderKey(int RockIndex, int ShiftIndex)
{
}