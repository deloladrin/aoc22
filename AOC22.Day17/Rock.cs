using System;
using System.Drawing;

namespace AOC22.Day17;

public class Rock
{
    public RockShape Shape { get; }
    public Point Point { get; set; }

    public Rock(RockShape shape, Point point)
    {
        Shape = shape;
        Point = point;
    }

    public void Settle(Room room)
    {
        foreach (Point point in Shape.Points)
        {
            Point real = new Point(Point.X + point.X, Point.Y + point.Y);
            room.Put(real);
        }
    }
}