using System;
using System.Collections.Generic;
using System.Drawing;

namespace AOC22.Day17;

public class Room
{
    public const int Width = 7;
    
    public List<char[]> Map { get; }

    public Room()
    {
        Map = new List<char[]>();
    }

    public bool this[int x, int y]
    {
        get
        {
            if (y < 0)
                return true;

            if (x < 0 || x >= Width)
                return true;
            
            if (y >= Map.Count)
                return false;

            return Map[y][x] == '#';
        }
    }

    public char[] AddRow()
    {
        char[] row = new char[Width];
        Map.Add(row);

        return row;
    }

    public int Height
    {
        get => Map.Count;
    }

    public void Put(Point point)
    {
        while (point.Y >= Map.Count)
            AddRow();

        Map[point.Y][point.X] = '#';
    }
}