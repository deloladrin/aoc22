using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AOC22.Day17;

public class RockShape
{
    public static readonly RockShape MinusShape = new RockShape(new List<Point>()
    {
        new Point(0, 0),
        new Point(1, 0),
        new Point(2, 0),
        new Point(3, 0)
    });
    
    public static readonly RockShape PlusShape = new RockShape(new List<Point>()
    {
        new Point(0, 1),
        new Point(1, 0),
        new Point(1, 1),
        new Point(1, 2),
        new Point(2, 1)
    });
    
    public static readonly RockShape LShape = new RockShape(new List<Point>()
    {
        new Point(0, 0),
        new Point(1, 0),
        new Point(2, 0),
        new Point(2, 1),
        new Point(2, 2)
    });
    
    public static readonly RockShape IShape = new RockShape(new List<Point>()
    {
        new Point(0, 0),
        new Point(0, 1),
        new Point(0, 2),
        new Point(0, 3)
    });
    
    public static readonly RockShape BoxShape = new RockShape(new List<Point>()
    {
        new Point(0, 0),
        new Point(0, 1),
        new Point(1, 0),
        new Point(1, 1)
    });

    public static readonly RockShape[] Shapes = new RockShape[]
    {
        MinusShape, PlusShape, LShape, IShape, BoxShape
    };
    
    public List<Point> Points { get; }
    
    public int MinX { get; }
    public int MaxX { get; }
    public int MinY { get; }
    public int MaxY { get; }

    public RockShape(List<Point> points)
    {
        Points = points;

        MinX = Points.Min(v => v.X);
        MaxX = Points.Max(v => v.X);
        MinY = Points.Min(v => v.Y);
        MaxY = Points.Max(v => v.Y);
    }

    public int Width
    {
        get => MaxX - MinX + 1;
    }
    
    public int Height
    {
        get => MaxY - MinY + 1;
    }
}