using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
      
namespace AOC22.Day17;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static bool CheckForCollision(Room room, Rock rock, Point next)
    {
        foreach (Point point in rock.Shape.Points)
        {
            Point real = new Point(next.X + point.X, next.Y + point.Y);

            if (room[real.X, real.Y])
                return true;
        }

        return false;
    }
    
    private static int Simulate(Room room, RockShape rockShapeType, string shifts, int shiftIndex)
    {
        Point spawn = new Point(2, room.Height + 3);
        Rock rock = new Rock(rockShapeType, spawn);

        int ticks = 0;

        while (true)
        {
            if (ticks % 2 == 0)
            {
                Point next = new Point();
                
                if (shifts[shiftIndex] == '>')
                    next = rock.Point + new Size(1, 0);

                if (shifts[shiftIndex] == '<')
                    next = rock.Point - new Size(1, 0);

                shiftIndex = (shiftIndex + 1) % shifts.Length;
                ticks++;

                if (CheckForCollision(room, rock, next))
                    continue;    
                    
                rock.Point = next;
            }
            
            else
            {
                Point next = rock.Point - new Size(0, 1);
                ticks++;

                if (CheckForCollision(room, rock, next))
                    break;

                rock.Point = next;
            }
        }

        rock.Settle(room);
        return shiftIndex;
    }
    
    private static void Task1(RockShape[] shapes, string shifts)
    {
        Room room = new Room();
        
        int rockIndex = 0;
        int shiftIndex = 0;
        
        for (int i = 0; i < 2022; i++)
        {
            shiftIndex = Simulate(room, shapes[rockIndex], shifts, shiftIndex);
            rockIndex = (rockIndex + 1) % shapes.Length;
        }

        Console.WriteLine($"Task1: {room.Height}");
    }
      
    private static void Task2(RockShape[] shapes, string shifts)
    {
        Room room = new Room();
        int i = 0;
        
        int rockIndex = 0;
        int shiftIndex = 0;

        Dictionary<RepetitionFinderKey, List<RepetitionFinderValue>> repetitions = new Dictionary<RepetitionFinderKey, List<RepetitionFinderValue>>();
        
        for (i = 0; i < 20000; i++)
        {
            RepetitionFinderKey key = new RepetitionFinderKey(rockIndex, shiftIndex);
            RepetitionFinderValue value = new RepetitionFinderValue(i, room.Height);

            if (!repetitions.ContainsKey(key))
                repetitions[key] = new List<RepetitionFinderValue>();
            
            repetitions[key].Add(value);
            
            shiftIndex = Simulate(room, shapes[rockIndex], shifts, shiftIndex);
            rockIndex = (rockIndex + 1) % shapes.Length;
        }

        KeyValuePair<RepetitionFinderKey, List<RepetitionFinderValue>> repetition = repetitions.OrderByDescending(x => x.Value.Count).FirstOrDefault();

        RepetitionFinderValue first = repetition.Value[^2];
        RepetitionFinderValue second = repetition.Value[^1];

        int distance = second.Index - first.Index;
        int difference = second.Height - first.Height;

        long amount = 1000000000000 - second.Index;
        long canSkip = amount / distance;
        long remaining = amount % distance;

        for (; i < second.Index + distance; i++)
        {
            shiftIndex = Simulate(room, shapes[rockIndex], shifts, shiftIndex);
            rockIndex = (rockIndex + 1) % shapes.Length;
        }

        int referenceHeight = room.Height;

        for (i = 0; i < remaining; i++)
        {
            shiftIndex = Simulate(room, shapes[rockIndex], shifts, shiftIndex);
            rockIndex = (rockIndex + 1) % shapes.Length;
        }

        long total = second.Height + canSkip * difference + room.Height - referenceHeight;
        Console.WriteLine($"Task2: {total}");
    }
          
    private static void Main(string[] args)
    {
        string shifts = File.ReadAllText(InputFile);
        RockShape[] shapes = RockShape.Shapes;

        Task1(shapes, shifts);
        Task2(shapes, shifts);
    }
}
