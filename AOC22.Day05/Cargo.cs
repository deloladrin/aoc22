using System;
using System.Collections.Generic;
using System.Linq;

namespace AOC22.Day05;

public record Cargo(List<Stack<char>> Stacks)
{
    public Cargo(string content) :
        this(new List<Stack<char>>())
    {
        foreach ((string line, int index) in content.Split("\n").Reverse().Skip(1).Select((x, i) => (x, i)))
        {
            int count = (line.Length + 1) / 4;
            
            if (index == 0)
            {
                for (int i = 0; i < count; i++)
                {
                    Stack<char> stack = new Stack<char>();
                    Stacks.Add(stack);
                }
            }

            for (int i = 0; i < count; i++)
            {
                char crate = line[i * 4 + 1];
                
                if (crate != ' ')
                    Stacks[i].Push(crate);
            }
        }
    }
}