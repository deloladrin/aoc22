using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day05;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static string GetTopCrates(Cargo cargo)
    {
        string result = string.Empty;

        foreach (Stack<char> stack in cargo.Stacks)
            result += stack.Peek();

        return result;
    }
    
    private static void Task1(Cargo cargo, List<Command> commands)
    {
        foreach (Command command in commands)
        {
            for (int i = 0; i < command.Amount; i++)
            {
                char crate = cargo.Stacks[command.Source - 1].Pop();
                cargo.Stacks[command.Destination - 1].Push(crate);
            }
        }
        
        Console.WriteLine($"Task1: {GetTopCrates(cargo)}");
    }
      
    private static void Task2(Cargo cargo, List<Command> commands)
    {
        foreach (Command command in commands)
        {
            Stack<char> temp = new Stack<char>();

            for (int i = 0; i < command.Amount; i++)
            {
                char crate = cargo.Stacks[command.Source - 1].Pop();
                temp.Push(crate);
            }
            
            for (int i = 0; i < command.Amount; i++)
            {
                char crate = temp.Pop();
                cargo.Stacks[command.Destination - 1].Push(crate);
            }
        }
        
        Console.WriteLine($"Task2: {GetTopCrates(cargo)}");
    }
          
    private static void Main(string[] args)
    {
        string[] parts = File.ReadAllText(InputFile).Split("\n\n");

        Cargo cargo1 = new Cargo(parts[0]);
        Cargo cargo2 = new Cargo(parts[0]);
        
        List<Command> commands = new List<Command>();

        foreach (string action in parts[1].Split("\n"))
        {
            Command command = new Command(action);
            commands.Add(command);
        }

        Task1(cargo1, commands);
        Task2(cargo2, commands);
    }
}
