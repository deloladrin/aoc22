using System;
using System.Text.RegularExpressions;

namespace AOC22.Day05;

public record Command(int Amount, int Source, int Destination)
{
    public static Regex Regex = new Regex(@"move (\d+) from (\d+) to (\d+)");
    
    public Command(string content) :
        this(0, 0, 0)
    {
        Match match = Regex.Match(content);

        if (match.Success)
        {
            Amount = int.Parse(match.Groups[1].Value);
            Source = int.Parse(match.Groups[2].Value);
            Destination = int.Parse(match.Groups[3].Value);
        }
    }
}