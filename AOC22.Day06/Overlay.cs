using System;
using System.Collections.Generic;

namespace AOC22.Day06;

public class Overlay
{
    public char[] Array { get; }

    public int Counter { get; private set; }

    public Overlay(int length)
    {
        Array = new char[length];
        Counter = 0;
    }

    public void Move(char next)
    {
        for (int i = Array.Length - 1; i >= 1; i--)
            Array[i] = Array[i - 1];
        
        Array[0] = next;
        Counter++;
    }

    public bool Check()
    {
        if (Counter < Array.Length)
            return false;
        
        for (int i = 0; i < Array.Length; i++)
            for (int j = i + 1; j < Array.Length; j++)
                if (Array[i] == Array[j])
                    return false;

        return true;
    }
}