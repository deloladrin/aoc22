using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day06;
      
public class Program
{
    private const string InputFile = "Input.txt";
          
    private static void Task1(string signal)
    {
        Overlay overlay = new Overlay(4);

        foreach (char c in signal)
        {
            overlay.Move(c);

            if (overlay.Check())
                break;
        }
        
        Console.WriteLine($"Task1: {overlay.Counter}");
    }
      
    private static void Task2(string signal)
    {
        Overlay overlay = new Overlay(14);

        foreach (char c in signal)
        {
            overlay.Move(c);

            if (overlay.Check())
                break;
        }
        
        Console.WriteLine($"Task2: {overlay.Counter}");
    }
          
    private static void Main(string[] args)
    {
        string signal = File.ReadAllText(InputFile);
        Task1(signal);
        Task2(signal);
    }
}
