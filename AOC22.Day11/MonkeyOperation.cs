using System;
using System.Text.RegularExpressions;

namespace AOC22.Day11;

public class MonkeyOperation
{
    private const int OldValue = int.MinValue;

    private static readonly Regex Regex = new Regex(@"new = ([0-9]+|old) ([+*]) ([0-9]+|old)");
    
    public int Left { get; }
    public char Operator { get; }
    public int Right { get; }

    public MonkeyOperation(string contents)
    {
        Match match = Regex.Match(contents);
        
        Left = LoadOperand(match.Groups[1].Value);
        Operator = match.Groups[2].Value[0];
        Right = LoadOperand(match.Groups[3].Value);
    }

    public long Apply(long value)
    {
        long left = GetOperandValue(Left, value);
        long right = GetOperandValue(Right, value);

        if (Operator == '+')
            return left + right;

        if (Operator == '*')
            return left * right;

        return 0;
    }
    
    private int LoadOperand(string value)
    {
        if (value == "old")
            return OldValue;

        return int.Parse(value);
    }

    private long GetOperandValue(int operand, long oldValue)
    {
        if (operand == OldValue)
            return oldValue;

        return operand;
    }
}