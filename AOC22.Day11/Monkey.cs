using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AOC22.Day11;

public class Monkey
{
    public int ID { get; }
    public Queue<long> Items { get; }
    public MonkeyOperation Operation { get; }
    public MonkeyTest Test { get; }
    
    public long Inspections { get; set; }

    public Monkey(Match match)
    {
        ID = int.Parse(match.Groups[1].Value);
        Items = LoadItems(match.Groups[2].Value);
        Operation = new MonkeyOperation(match.Groups[3].Value);
        Test = new MonkeyTest(match.Groups[4].Value, match.Groups[5].Value, match.Groups[6].Value);

        Inspections = 0;
    }

    public void Play(List<Monkey> monkeys)
    {
        while (Items.Count > 0)
        {
            long item = Items.Dequeue();
            
            item = Operation.Apply(item);
            item /= 3;

            int id = Test.Apply(item);
            monkeys[id].Items.Enqueue(item);

            Inspections++;
        }
    }

    public void Play(List<Monkey> monkeys, long lcm)
    {
        while (Items.Count > 0)
        {
            long item = Items.Dequeue();
            
            item = Operation.Apply(item);
            item %= lcm;

            int id = Test.Apply(item);
            monkeys[id].Items.Enqueue(item);

            Inspections++;
        }
    }

    private Queue<long> LoadItems(string contents)
    {
        Queue<long> result = new Queue<long>();
        
        foreach (string entry in contents.Split(", "))
        {
            long value = long.Parse(entry);
            result.Enqueue(value);
        }

        return result;
    }
}