using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AOC22.Day11;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static readonly Regex Regex = new Regex(@"Monkey ([0-9]+):\n. Starting items: ([0-9]+(?:, [0-9]+)*)\n. Operation: (.*)\n. Test: divisible by ([0-9]+)\n.   If true: throw to monkey ([0-9]+)\n.   If false: throw to monkey ([0-9])");
          
    private static void Task1(List<Monkey> monkeys)
    {
        for (int i = 0; i < 20; i++)
            foreach (Monkey monkey in monkeys)
                monkey.Play(monkeys);

        List<Monkey> ordered = monkeys.OrderByDescending(x => x.Inspections).ToList();
        long total = ordered[0].Inspections * ordered[1].Inspections;
        
        Console.WriteLine($"Task1: {total}");
    }

    private static long GCD(long a, long b)
    {
        if (b == 0)
            return a;

        return GCD(b, a % b);
    }
    
    private static long LCM(long a, long b)
    {
        return (a / GCD(a, b)) * b;
    }
    
    private static long LCM(List<Monkey> monkeys)
    {
        long current = 1;

        foreach (Monkey monkey in monkeys)
            current = LCM(current, monkey.Test.Divisor);

        return current;
    }
      
    private static void Task2(List<Monkey> monkeys)
    {
        long lcm = LCM(monkeys);
        
        for (int i = 0; i < 10000; i++)
            foreach (Monkey monkey in monkeys)
                monkey.Play(monkeys, lcm);

        List<Monkey> ordered = monkeys.OrderByDescending(x => x.Inspections).ToList();
        long total = ordered[0].Inspections * ordered[1].Inspections;
        
        Console.WriteLine($"Task2: {total}");
    }
          
    private static void Main(string[] args)
    {
        List<Monkey> monkeys1 = new List<Monkey>();
        List<Monkey> monkeys2 = new List<Monkey>();
        
        string content = File.ReadAllText(InputFile);
        
        foreach (Match match in Regex.Matches(content))
        {
            Monkey monkey1 = new Monkey(match);
            monkeys1.Add(monkey1);
            
            Monkey monkey2 = new Monkey(match);
            monkeys2.Add(monkey2);
        }
        
        Task1(monkeys1);
        Task2(monkeys2);
    }
}
