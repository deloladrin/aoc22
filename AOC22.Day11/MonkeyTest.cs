using System;

namespace AOC22.Day11;

public class MonkeyTest
{
    public int Divisor { get; }
    public int TrueTarget { get; }
    public int FalseTarget { get; }

    public MonkeyTest(string divisor, string trueTarget, string falseTarget)
    {
        Divisor = int.Parse(divisor);
        TrueTarget = int.Parse(trueTarget);
        FalseTarget = int.Parse(falseTarget);
    }

    public int Apply(long value)
    {
        if (value % Divisor == 0)
            return TrueTarget;

        return FalseTarget;
    }
}