using System;
using System.Collections.Generic;

namespace AOC22.Day13;

public class ListEntry : IEntry
{
    public List<IEntry> Entries { get; }
    
    public ListEntry(List<IEntry> entries)
    {
        Entries = entries;
    }

    public static int Create(string contents, out ListEntry entry)
    {
        List<IEntry> entries = new List<IEntry>();
        int index = 1;

        while (contents[index] != ']')
        {
            if (contents[index] >= '0' && contents[index] <= '9')
            {
                index += IntEntry.Create(contents[index..], out IntEntry integer);
                entries.Add(integer);
                continue;
            }
            
            if (contents[index] == ',')
            {
                index++;
                continue;
            }

            if (contents[index] == '[')
            {
                index += ListEntry.Create(contents[index..], out ListEntry list);
                entries.Add(list);
                continue;
            }
        }

        index++;

        entry = new ListEntry(entries);
        return index;
    }
}