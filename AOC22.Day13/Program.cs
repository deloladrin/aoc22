using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day13;
      
public class Program
{
    private const string InputFile = "Input.txt";
          
    private static int Compare(IEntry left, IEntry right)
    {
        if (left is IntEntry leftInt && right is IntEntry rightInt)
        {
            return leftInt.Value.CompareTo(rightInt.Value);
        }

        if (left is ListEntry leftList && right is ListEntry rightList)
        {
            for (int i = 0; i < leftList.Entries.Count; i++)
            {
                if (i >= rightList.Entries.Count)
                    return 1;

                int result = Compare(leftList.Entries[i], rightList.Entries[i]);

                if (result != 0)
                    return result;
            }

            if (leftList.Entries.Count < rightList.Entries.Count)
                return -1;
        }

        if (left is ListEntry leftList2 && right is IntEntry rightInt2)
        {
            List<IEntry> filler = new List<IEntry>() { rightInt2 };
            ListEntry rightList2 = new ListEntry(filler);

            return Compare(leftList2, rightList2);
        }

        if (left is IntEntry leftInt3 && right is ListEntry rightList3)
        {
            List<IEntry> filler = new List<IEntry>() { leftInt3 };
            ListEntry leftList3 = new ListEntry(filler);

            return Compare(leftList3, rightList3);
        }

        return 0;
    }
    
    private static void Task1(List<Pair> pairs)
    {
        int total = 0;

        for (int i = 0; i < pairs.Count; i++)
        {
            if (Compare(pairs[i].Left, pairs[i].Right) == -1)
                total += i + 1;
        }
        
        Console.WriteLine($"Task1: {total}");
    }
      
    private static void Task2(List<Pair> pairs)
    {
        List<IEntry> entries = new List<IEntry>();

        foreach (Pair pair in pairs)
        {
            entries.Add(pair.Left);
            entries.Add(pair.Right);
        }

        List<IEntry> divEntries1 = new List<IEntry>() { new IntEntry(2) };
        ListEntry div1 = new ListEntry(divEntries1);
        entries.Add(div1);

        List<IEntry> divEntries2 = new List<IEntry>() { new IntEntry(6) };
        ListEntry div2 = new ListEntry(divEntries2);
        entries.Add(div2);
        
        entries.Sort(Compare);

        int div1Index = entries.IndexOf(div1) + 1;
        int div2Index = entries.IndexOf(div2) + 1;

        Console.WriteLine($"Task2: {div1Index * div2Index}");
    }
          
    private static void Main(string[] args)
    {
        List<Pair> pairs = new List<Pair>();
        
        foreach (string content in File.ReadAllText(InputFile).Split("\n\n"))
        {
            string[] parts = content.Split('\n');

            ListEntry.Create(parts[0], out ListEntry left);
            ListEntry.Create(parts[1], out ListEntry right);

            Pair pair = new Pair(left, right);
            pairs.Add(pair);
        }
        
        Task1(pairs);
        Task2(pairs);
    }
}
