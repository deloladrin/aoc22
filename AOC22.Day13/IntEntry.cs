using System;

namespace AOC22.Day13;

public class IntEntry : IEntry
{
    public int Value { get; }
    
    public IntEntry(int value)
    {
        Value = value;
    }

    public static int Create(string contents, out IntEntry entry)
    {
        int firstIndexOfComma = contents.IndexOf(',');
        int firstIndexOfBracket = contents.IndexOf(']');

        int lastIndex = Math.Min(firstIndexOfComma, firstIndexOfBracket);

        if (firstIndexOfComma == -1)
            lastIndex = firstIndexOfBracket;

        if (firstIndexOfBracket == -1)
            lastIndex = firstIndexOfComma;
        
        int value = int.Parse(contents[..lastIndex]);

        entry = new IntEntry(value);
        return lastIndex;
    }
}