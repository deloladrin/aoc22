using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
      
namespace AOC22.Day15;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static Rectangle GetSensorBounds(List<Sensor> sensors)
    {
        int minX = int.MaxValue;
        int maxX = int.MinValue;

        int minY = int.MaxValue;
        int maxY = int.MinValue;

        foreach (Sensor sensor in sensors)
        {
            if (sensor.Position.X - sensor.Distance < minX)
                minX = sensor.Position.X - sensor.Distance;
            
            if (sensor.Position.X + sensor.Distance > maxX)
                maxX = sensor.Position.X + sensor.Distance;
            
            if (sensor.Position.Y - sensor.Distance < minY)
                minY = sensor.Position.Y - sensor.Distance;
            
            if (sensor.Position.Y + sensor.Distance > maxY)
                maxY = sensor.Position.Y + sensor.Distance;
        }

        return new Rectangle(minX, minY, maxX - minX + 1, maxY - minY + 1);
    }
    
    private static void Task1(List<Sensor> sensors)
    {
        int total = 0;
        Rectangle bounds = GetSensorBounds(sensors);
        
        for (int x = bounds.Left; x <= bounds.Right; x++)
        {
            Point point = new Point(x, 2000000);
            
            foreach (Sensor sensor in sensors)
            {
                if (sensor.IsCovered(point, true))
                {
                    total++;
                    break;
                }
            }
        }
        
        Console.WriteLine($"Task1: {total}");
    }
      
    private static void Task2(List<Sensor> sensors)
    {
        Point possible = Point.Empty;

        foreach (Sensor sensor in sensors)
        {
            foreach (Point point in sensor.GetSurroundingPoints())
            {
                if (point.X >= 0 && point.X <= 4000000 &&
                    point.Y >= 0 && point.Y <= 4000000)
                {
                    bool covered = false;

                    foreach (Sensor checker in sensors)
                    {
                        if (checker.IsCovered(point, false))
                        {
                            covered = true;
                            break;
                        }
                    }

                    if (!covered)
                    {
                        possible = point;
                        goto exit;
                    }
                }
            }
        }

        exit:
        long frequency = 4000000L * possible.X + possible.Y;
        
        Console.WriteLine($"Task2: {frequency}");
    }
          
    private static void Main(string[] args)
    {
        List<Sensor> sensors = new List<Sensor>();

        foreach (string line in File.ReadAllLines(InputFile))
        {
            Sensor sensor = new Sensor(line);
            sensors.Add(sensor);
        }

        Task1(sensors);
        Task2(sensors);
    }
}
