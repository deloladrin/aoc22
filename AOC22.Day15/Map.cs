using System;
using System.Collections.Generic;

namespace AOC22.Day15;

public class Map
{
    private char[,] _internal;

    private int _minX;
    private int _maxX;
    private int _minY;
    private int _maxY;

    public Map(List<Sensor> sensors)
    {
        this._minX = int.MaxValue;
        this._maxX = int.MinValue;

        this._minY = int.MaxValue;
        this._maxY = int.MinValue;

        foreach (Sensor sensor in sensors)
        {
            if (sensor.Position.X < this._minX)
                this._minX = sensor.Position.X;
            
            if (sensor.Beacon.X < this._minX)
                this._minX = sensor.Beacon.X;
            
            if (sensor.Position.X > this._maxX)
                this._maxX = sensor.Position.X;
            
            if (sensor.Beacon.X > this._maxX)
                this._maxX = sensor.Beacon.X;
            
            if (sensor.Position.Y < this._minY)
                this._minY = sensor.Position.Y;
            
            if (sensor.Beacon.Y < this._minY)
                this._minY = sensor.Beacon.Y;
            
            if (sensor.Position.Y > this._maxY)
                this._maxY = sensor.Position.Y;
            
            if (sensor.Beacon.Y > this._maxY)
                this._maxY = sensor.Beacon.Y;
        }
        
        this._internal = new char[this._maxX - this._minX + 1, this._maxY - this._minY + 1];
        
        for (int y = 0; y < this._internal.GetLength(1); y++)
            for (int x = 0; x < this._internal.GetLength(0); x++)
                this._internal[x, y] = ' ';
    }

    public char this[int x, int y]
    {
        get => this._internal[x - this._minX, y - this._minY];
        set => this._internal[x - this._minX, y - this._minY] = value;
    }

    public bool InBounds(int x, int y)
    {
        return x >= this._minX && x <= this._maxX &&
               y >= this._minY && y <= this._maxY;
    }

    public void Print()
    {
        for (int y = 0; y < this._internal.GetLength(1); y++)
        {
            for (int x = 0; x < this._internal.GetLength(0); x++)
                Console.Write(this._internal[x, y]);
            
            Console.WriteLine();
        }
    }

    public int MinX
    {
        get => this._minX;
    }
    
    public int MinY
    {
        get => this._minY;
    }

    public int MaxX
    {
        get => this._maxX;
    }
    
    public int MaxY
    {
        get => this._maxY;
    }
}