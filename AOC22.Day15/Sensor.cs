using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;

namespace AOC22.Day15;

public record Sensor(Point Position, Point Beacon)
{
    public static readonly Regex Regex = new Regex(@"Sensor at x=([-0-9]+), y=([-0-9]+): closest beacon is at x=([-0-9]+), y=([-0-9]+)");

    public int Distance { get; }
    
    public Sensor(string contents) :
        this(Point.Empty, Point.Empty)
    {
        Match match = Regex.Match(contents);

        int x = int.Parse(match.Groups[1].Value);
        int y = int.Parse(match.Groups[2].Value);
        Position = new Point(x, y);

        int bx = int.Parse(match.Groups[3].Value);
        int by = int.Parse(match.Groups[4].Value);
        Beacon = new Point(bx, by);

        Distance = GetDistance(Beacon);
    }

    public int GetDistance(Point point)
    {
        return Math.Abs(Position.X - point.X) + Math.Abs(Position.Y - point.Y);
    }

    public HashSet<Point> GetSurroundingPoints()
    {
        HashSet<Point> points = new HashSet<Point>();
        int distance = Distance + 1;
        
        for (int x = -distance; x <= distance; x++)
        {
            int remaining = distance - Math.Abs(x);

            Point top = new Point(Position.X + x, Position.Y + remaining);
            points.Add(top);

            Point bot = new Point(Position.X + x, Position.Y - remaining);
            points.Add(bot);
        }

        return points;
    }

    public bool IsCovered(Point point, bool skipOwnBeacon)
    {
        return GetDistance(point) <= Distance &&
               (!skipOwnBeacon || point != Beacon);
    }
}