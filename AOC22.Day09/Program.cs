using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
      
namespace AOC22.Day09;
      
public static class Program
{
    private const string InputFile = "Input.txt";

    private static Point Move(this Point pt, MoveDirection direction)
    {
        return direction switch
        {
            MoveDirection.U => new Point(pt.X, pt.Y + 1),
            MoveDirection.R => new Point(pt.X + 1, pt.Y),
            MoveDirection.D => new Point(pt.X, pt.Y - 1),
            MoveDirection.L => new Point(pt.X - 1, pt.Y),
            
            _ => pt
        };
    }

    private static double Distance(Point pt1, Point pt2)
    {
        double dx = pt1.X - pt2.X;
        double dy = pt1.Y - pt2.Y;
        return Math.Sqrt(dx * dx + dy * dy);
    }

    private static Point Follow(this Point tail, Point head)
    {
        if (Distance(tail, head) > 1.5)
        {
            if (head.X == tail.X && head.Y > tail.Y)
                return new Point(tail.X, tail.Y + 1);

            if (head.X == tail.X && head.Y < tail.Y)
                return new Point(tail.X, tail.Y - 1);
            
            if (head.X > tail.X && head.Y == tail.Y)
                return new Point(tail.X + 1, tail.Y);

            if (head.X < tail.X && head.Y == tail.Y)
                return new Point(tail.X - 1, tail.Y);

            if (head.X > tail.X && head.Y > tail.Y)
                return new Point(tail.X + 1, tail.Y + 1);
            
            if (head.X < tail.X && head.Y > tail.Y)
                return new Point(tail.X - 1, tail.Y + 1);
            
            if (head.X > tail.X && head.Y < tail.Y)
                return new Point(tail.X + 1, tail.Y - 1);
            
            if (head.X < tail.X && head.Y < tail.Y)
                return new Point(tail.X - 1, tail.Y - 1);
        }

        return tail;
    }
    
    private static void Task1(List<Move> moves)
    {
        Point head = new Point(0, 0);
        Point tail = new Point(0, 0);
        
        HashSet<Point> visited = new HashSet<Point>();

        foreach (Move move in moves)
        {
            for (int i = 0; i < move.Amount; i++)
            {
                head = head.Move(move.Direction);
                tail = tail.Follow(head);

                visited.Add(tail);
            }
        }
        
        Console.WriteLine($"Task1: {visited.Count}");
    }
      
    private static void Task2(List<Move> moves)
    {
        Point[] points = new Point[10];
        HashSet<Point> visited = new HashSet<Point>();

        foreach (Move move in moves)
        {
            for (int i = 0; i < move.Amount; i++)
            {
                points[0] = points[0].Move(move.Direction);

                for (int j = 1; j < points.Length; j++)
                    points[j] = points[j].Follow(points[j - 1]);

                Point last = points.Last();
                visited.Add(last);
            }
        }
        
        Console.WriteLine($"Task2: {visited.Count}");
    }
          
    private static void Main(string[] args)
    {
        List<Move> moves = new List<Move>();

        foreach (string line in File.ReadAllLines(InputFile))
        {
            Move move = new Move(line);
            moves.Add(move);
        }

        Task1(moves);
        Task2(moves);
    }
}
