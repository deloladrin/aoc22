using System;

namespace AOC22.Day09;

public enum MoveDirection
{
    U,
    R,
    D,
    L
}