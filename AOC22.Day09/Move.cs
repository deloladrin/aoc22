using System;

namespace AOC22.Day09;

public record Move(MoveDirection Direction, int Amount)
{
    public Move(string contents) :
        this(0, 0)
    {
        Direction = Enum.Parse<MoveDirection>(contents[..1]);
        Amount = int.Parse(contents[2..]);
    }
}