namespace AOC22.Day04;

public record Assignment(int Start, int End)
{
    public Assignment(string contents) :
        this(0, 0)
    {
        string[] parts = contents.Split('-');
        Start = int.Parse(parts[0]);
        End = int.Parse(parts[1]);
    }
}