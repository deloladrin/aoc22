namespace AOC22.Day04;

public record Pair(Assignment Elf1, Assignment Elf2)
{
    public Pair(string contents) :
        this(null!, null!)
    {
        string[] parts = contents.Split(',');
        Elf1 = new Assignment(parts[0]);
        Elf2 = new Assignment(parts[1]);
    }
}