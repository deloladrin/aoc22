using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day04;
      
public class Program
{
    private const string InputFile = "Input.txt";
          
    private static void Task1(List<Pair> pairs)
    {
        int total = 0;

        foreach (Pair pair in pairs)
        {
            if ((pair.Elf1.Start >= pair.Elf2.Start && pair.Elf1.End <= pair.Elf2.End) ||
                (pair.Elf2.Start >= pair.Elf1.Start && pair.Elf2.End <= pair.Elf1.End))
                total++;
        }
        
        Console.WriteLine($"Task1: {total}");
    }
      
    private static void Task2(List<Pair> pairs)
    {
        int total = 0;

        foreach (Pair pair in pairs)
        {
            if ((pair.Elf1.End >= pair.Elf2.Start && pair.Elf1.Start <= pair.Elf2.End) ||
                (pair.Elf2.End >= pair.Elf1.Start && pair.Elf2.Start <= pair.Elf1.End))
                total++;
        }
        
        Console.WriteLine($"Task2: {total}");
    }
          
    private static void Main(string[] args)
    {
        List<Pair> pairs = new List<Pair>();

        foreach (string line in File.ReadAllLines(InputFile))
        {
            Pair pair = new Pair(line);
            pairs.Add(pair);
        }

        Task1(pairs);
        Task2(pairs);
    }
}
