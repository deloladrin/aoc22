using System;
using System.Collections.Generic;
using System.Drawing;

namespace AOC22.Day14;

public record Wall(List<Point> Points)
{
    public Wall(string contents) :
        this(new List<Point>())
    {
        Point? last = null;

        foreach (string position in contents.Split(" -> "))
        {
            string[] parts = position.Split(',');
            int x = int.Parse(parts[0]);
            int y = int.Parse(parts[1]);

            Point point = new Point(x, y);

            if (last is not null)
            {
                Size direction = GetDirection(last.Value, point);
                Point current = last.Value;

                while (current != point)
                {
                    Points.Add(current);
                    current += direction;
                }
            }

            last = point;
        }
        
        if (last.HasValue)
            Points.Add(last.Value);
    }

    private Size GetDirection(Point src, Point dst)
    {
        if (dst.X > src.X) return new Size(1, 0);
        if (dst.X < src.X) return new Size(-1, 0);
        if (dst.Y > src.Y) return new Size(0, 1);
        if (dst.Y < src.Y) return new Size(0, -1);

        return Size.Empty;
    }
}