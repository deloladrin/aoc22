using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
      
namespace AOC22.Day14;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static bool Simulate(char[,] map, Point source)
    {
        Point current = source;

        while (current.Y < map.GetLength(1) - 1)
        {
            if (map[current.X, current.Y + 1] == ' ')
            {
                current.Y++;
                continue;
            }

            if (map[current.X - 1, current.Y + 1] == ' ')
            {
                current.X--;
                current.Y++;
                continue;
            }

            if (map[current.X + 1, current.Y + 1] == ' ')
            {
                current.X++;
                current.Y++;
                continue;
            }

            map[current.X, current.Y] = 'O';

            if (current == source)
                return false;
            
            return true;
        }

        return false;
    }
    
    private static void Task1(char[,] map, Point source)
    {
        int total = 0;

        while (Simulate(map, source))
            total++;

        Console.WriteLine($"Task1: {total}");
    }

    private static void Task2(char[,] map, Point source)
    {
        int total = 0;

        for (int x = 0; x < map.GetLength(0); x++)
            map[x, map.GetLength(1) - 1] = '█';

        while (Simulate(map, source))
            total++;

        total++;
        Console.WriteLine($"Task2: {total}");
    }

    private static void InitializeArray(char[,] array)
    {
        for (int x = 0; x < array.GetLength(0); x++)
            for (int y = 0; y < array.GetLength(1); y++)
                array[x, y] = ' ';
    }
          
    private static void Main(string[] args)
    {
        List<Wall> walls = new List<Wall>();

        foreach (string line in File.ReadAllLines(InputFile))
        {
            Wall wall = new Wall(line);
            walls.Add(wall);
        }

        int maxX = walls.Max(a => a.Points.Max(b => b.X));
        int maxY = walls.Max(a => a.Points.Max(b => b.Y));
        
        char[,] map1 = new char[maxX + 1, maxY + 1];
        InitializeArray(map1);
        
        char[,] map2 = new char[maxX + 100, maxY + 3];
        InitializeArray(map2);

        foreach (Wall wall in walls)
        {
            foreach (Point point in wall.Points)
            {
                map1[point.X, point.Y] = '█';
                map2[point.X, point.Y] = '█';
            }
        }

        Point source = new Point(500, 0);

        Task1(map1, source);
        Task2(map2, source);
    }
}
