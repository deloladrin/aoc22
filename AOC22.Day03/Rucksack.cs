using System.Collections.Generic;
using System.Linq;

namespace AOC22.Day03;

public record Rucksack(List<char> First, List<char> Second)
{
    public Rucksack(string contents) :
        this(null!, null!)
    {
        int size = contents.Length / 2;
        First = contents[..size].ToList();
        Second = contents[size..].ToList();
    }
}