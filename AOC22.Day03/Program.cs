using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day03;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static int LetterValue(char letter)
    {
        if (letter >= 'a' && letter <= 'z')
            return letter - 'a' + 1;

        if (letter >= 'A' && letter <= 'Z')
            return letter - 'A' + 27;

        return 0;
    }
    
    private static void Task1(List<Rucksack> rucksacks)
    {
        int total = 0;

        foreach (Rucksack rucksack in rucksacks)
        {
            char common = rucksack.First.Intersect(rucksack.Second).First();
            total += LetterValue(common);
        }
        
        Console.WriteLine($"Task1: {total}");
    }
      
    private static void Task2(List<Rucksack> rucksacks)
    {
        int total = 0;

        for (int i = 2; i < rucksacks.Count; i += 3)
        {
            Rucksack e1 = rucksacks[i - 2];
            Rucksack e2 = rucksacks[i - 1];
            Rucksack e3 = rucksacks[i];
            
            char common = e1.First.Union(e1.Second)
                .Intersect(e2.First.Union(e2.Second))
                .Intersect(e3.First.Union(e3.Second))
                .First();
            
            total += LetterValue(common);
        }
        
        Console.WriteLine($"Task2: {total}");
    }
          
    private static void Main(string[] args)
    {
        List<Rucksack> rucksacks = new List<Rucksack>();

        foreach (string line in File.ReadAllLines(InputFile))
        {
            Rucksack rucksack = new Rucksack(line);
            rucksacks.Add(rucksack);
        }
        
        Task1(rucksacks);
        Task2(rucksacks);
    }
}
