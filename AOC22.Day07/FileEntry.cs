using System;

namespace AOC22.Day07;

public record FileEntry(string Name, long Size) : IFileSystemEntry;