using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day07;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static long DirectorySizesBelow(DirectoryEntry directory, long limit)
    {
        long total = 0;
        long size = directory.Size;

        if (size <= limit)
            total += size;

        foreach (IFileSystemEntry entry in directory.Entries)
        {
            if (entry is DirectoryEntry dirEntry)
                total += DirectorySizesBelow(dirEntry, limit);
        }

        return total;
    }

    private static void Task1(FileSystem fs)
    {
        long total = DirectorySizesBelow(fs.Root, 100000);
        Console.WriteLine($"Task1: {total}");
    }
    
    private static DirectoryEntry DirectoryWithSizeAbove(DirectoryEntry directory, long limit)
    {
        DirectoryEntry chosen = directory;
        
        foreach (IFileSystemEntry entry in directory.Entries)
        {
            if (entry is DirectoryEntry dirEntry && entry.Size >= limit && entry.Size <= chosen.Size)
                chosen = DirectoryWithSizeAbove(dirEntry, limit);
        }

        return chosen;
    }
      
    private static void Task2(FileSystem fs)
    {
        DirectoryEntry delete = DirectoryWithSizeAbove(fs.Root, 30000000 - (70000000 - fs.Root.Size));
        Console.WriteLine($"Task2: {delete.Size}");
    }
          
    private static void Main(string[] args)
    {
        string[] lines = File.ReadAllLines(InputFile);
        FileSystem fs = new FileSystem(lines);
        
        Task1(fs);
        Task2(fs);
    }
}
