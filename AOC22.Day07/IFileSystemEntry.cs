using System;

namespace AOC22.Day07;

public interface IFileSystemEntry
{
    string Name { get; }
    long Size { get; }
}