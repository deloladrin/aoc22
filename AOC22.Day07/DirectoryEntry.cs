using System;
using System.Collections.Generic;

namespace AOC22.Day07;

public record DirectoryEntry(string Name, DirectoryEntry Parent, List<IFileSystemEntry> Entries) : IFileSystemEntry
{
    public DirectoryEntry(string Name, DirectoryEntry Parent) :
        this(Name, Parent, new List<IFileSystemEntry>())
    {
    }

    public long Size
    {
        get
        {
            long total = 0;

            foreach (IFileSystemEntry entry in Entries)
                total += entry.Size;

            return total;
        }
    }
}