using System;
using System.Text.RegularExpressions;

namespace AOC22.Day07;

public class FileSystem
{
    private static readonly Regex CommandCD = new Regex(@"^\$ cd (.*)$");
    private static readonly Regex CommandLS = new Regex(@"^\$ ls$");

    private static readonly Regex EntryFILE = new Regex(@"(\d+) (.*)");
    private static readonly Regex EntryDIRECTORY = new Regex(@"dir (.*)");

    public DirectoryEntry Root { get; }

    public FileSystem(string[] commands)
    {
        Root = new DirectoryEntry("/", null!);
        DirectoryEntry current = Root;

        bool lsExecution = false;

        foreach (string cmd in commands)
        {
            Match match;

            if (lsExecution)
            {
                if (!cmd.StartsWith("$"))
                {
                    if ((match = EntryFILE.Match(cmd)).Success)
                    {
                        FileEntry entry = new FileEntry(match.Groups[2].Value, long.Parse(match.Groups[1].Value));
                        current.Entries.Add(entry);
                        
                        continue;
                    }

                    if ((match = EntryDIRECTORY.Match(cmd)).Success)
                    {
                        DirectoryEntry entry = new DirectoryEntry(match.Groups[1].Value, current);
                        current.Entries.Add(entry);
                        
                        continue;
                    }
                }
            }

            if ((match = CommandCD.Match(cmd)).Success)
            {
                if (match.Groups[1].Value == "..")
                {
                    current = current.Parent;
                    continue;
                }
                
                foreach (IFileSystemEntry entry in current.Entries)
                {
                    if (entry.Name == match.Groups[1].Value && entry is DirectoryEntry directory)
                    {
                        current = directory;
                        break;
                    }
                }

                continue;
            }

            if ((match = CommandLS.Match(cmd)).Success)
            {
                lsExecution = true;
                continue;
            }
        }
    }
}