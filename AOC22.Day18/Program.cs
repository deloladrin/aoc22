using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
      
namespace AOC22.Day18;
      
public class Program
{
    private const string InputFile = "Input.txt";
    
    private static List<Face3D> GetUniqueFaces(Dictionary<Cube3D, Face3D[]> faces)
    {
        List<Face3D> unique = new List<Face3D>();

        foreach (KeyValuePair<Cube3D, Face3D[]> a in faces)
        {
            foreach (Face3D face in a.Value)
            {
                bool duplicate = false;
                
                foreach (KeyValuePair<Cube3D, Face3D[]> b in faces)
                {
                    if (a.Key == b.Key)
                        continue;

                    if (b.Value.Contains(face))
                    {
                        duplicate = true;
                        break;
                    }
                }
                
                if (!duplicate)
                    unique.Add(face);
            }
        }

        return unique;
    }
    
    private static void Task1(Dictionary<Cube3D, Face3D[]> faces)
    {
        List<Face3D> unique = GetUniqueFaces(faces);
        Console.WriteLine($"Task1: {unique.Count}");
    }

    private static Box3D GetBoundingBox(Dictionary<Cube3D, Face3D[]> faces)
    {
        int minX = int.MaxValue;
        int minY = int.MaxValue;
        int minZ = int.MaxValue;
        
        int maxX = int.MinValue;
        int maxY = int.MinValue;
        int maxZ = int.MinValue;

        foreach (Cube3D cube in faces.Keys)
        {
            minX = Math.Min(minX, cube.X);
            minY = Math.Min(minY, cube.Y);
            minZ = Math.Min(minZ, cube.Z);
            
            maxX = Math.Max(maxX, cube.X);
            maxY = Math.Max(maxY, cube.Y);
            maxZ = Math.Max(maxZ, cube.Z);
        }

        return new Box3D(minX, minY, minZ, maxX, maxY, maxZ);
    }

    private static bool CanEscapeBounds(Dictionary<Cube3D, Face3D[]> faces, Cube3D cube, Box3D bounds)
    {
        List<Cube3D> visited = new List<Cube3D>();

        while (!bounds.IsOutside(cube))
        {
            visited.Add(cube);
            
            Cube3D left = new Cube3D(cube.X - 1, cube.Y, cube.Z);
            if (!faces.ContainsKey(left) && !visited.Contains(left))
            {
                cube = left;
                continue;
            }
            
            Cube3D right = new Cube3D(cube.X + 1, cube.Y, cube.Z);
            if (!faces.ContainsKey(right) && !visited.Contains(right))
            {
                cube = right;
                continue;
            }
            
            Cube3D bottom = new Cube3D(cube.X, cube.Y - 1, cube.Z);
            if (!faces.ContainsKey(bottom) && !visited.Contains(bottom))
            {
                cube = bottom;
                continue;
            }
            
            Cube3D top = new Cube3D(cube.X, cube.Y + 1, cube.Z);
            if (!faces.ContainsKey(top) && !visited.Contains(top))
            {
                cube = top;
                continue;
            }

            Cube3D forward = new Cube3D(cube.X, cube.Y, cube.Z - 1);
            if (!faces.ContainsKey(forward) && !visited.Contains(forward))
            {
                cube = forward;
                continue;
            }
            
            Cube3D back = new Cube3D(cube.X, cube.Y, cube.Z + 1);
            if (!faces.ContainsKey(back) && !visited.Contains(back))
            {
                cube = back;
                continue;
            }

            int index = visited.IndexOf(cube);

            if (index == 0)
                return false;

            cube = visited[index - 1];
        }

        return true;
    }

    private static void Task2(Dictionary<Cube3D, Face3D[]> faces)
    {
        Box3D bounds = GetBoundingBox(faces);
        Dictionary<Cube3D, Face3D[]> fillers = new Dictionary<Cube3D, Face3D[]>();

        for (int z = bounds.MinZ + 1; z <= bounds.MaxZ - 1; z++)
        {
            for (int y = bounds.MinY + 1; y <= bounds.MaxY - 1; y++)
            {
                for (int x = bounds.MinX + 1; x <= bounds.MaxX - 1; x++)
                {
                    Cube3D cube = new Cube3D(x, y, z);

                    if (!CanEscapeBounds(faces, cube, bounds))
                    {
                        Face3D[] filler = cube.GetFaces();
                        fillers[cube] = filler;
                    }
                }
            }
        }

        foreach (KeyValuePair<Cube3D, Face3D[]> filler in fillers)
            faces[filler.Key] = filler.Value;
        
        List<Face3D> unique = GetUniqueFaces(faces);
        Console.WriteLine($"Task2: {unique.Count}");
    }
          
    private static void Main(string[] args)
    {
        List<Cube3D> cubes = new List<Cube3D>();

        foreach (string line in File.ReadAllLines(InputFile))
        {
            Cube3D cube = new Cube3D(line);
            cubes.Add(cube);
        }

        Dictionary<Cube3D, Face3D[]> faces = new Dictionary<Cube3D, Face3D[]>();

        foreach (Cube3D cube in cubes)
        {
            Face3D[] face = cube.GetFaces();
            faces[cube] = face;
        }
        
        Task1(faces);
        Task2(faces);
    }
}
