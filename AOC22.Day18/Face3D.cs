namespace AOC22.Day18;

public record struct Face3D(Cube3D From, Cube3D To)
{
}