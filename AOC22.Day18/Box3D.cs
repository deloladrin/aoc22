namespace AOC22.Day18;

public record struct Box3D(int MinX, int MinY, int MinZ, int MaxX, int MaxY, int MaxZ)
{
    public bool IsOutside(Cube3D cube)
    {
        return cube.X < MinX || cube.X > MaxX ||
               cube.Y < MinY || cube.Y > MaxY ||
               cube.Z < MinZ || cube.Z > MaxZ;
    }
}