using System.Collections.Generic;

namespace AOC22.Day18;

public record struct Cube3D(int X, int Y, int Z)
{
    public Cube3D(string contents) :
        this(0, 0, 0)
    {
        string[] parts = contents.Split(',');
        X = int.Parse(parts[0]);
        Y = int.Parse(parts[1]);
        Z = int.Parse(parts[2]);
    }

    public Face3D[] GetFaces()
    {
        return new Face3D[]
        {
            new Face3D(new Cube3D(X, Y, Z), new Cube3D(X + 1, Y + 1, Z)),
            new Face3D(new Cube3D(X, Y, Z), new Cube3D(X, Y + 1, Z + 1)),
            new Face3D(new Cube3D(X, Y, Z), new Cube3D(X + 1, Y, Z + 1)),
            new Face3D(new Cube3D(X, Y + 1, Z), new Cube3D(X + 1, Y + 1, Z + 1)),
            new Face3D(new Cube3D(X, Y, Z + 1), new Cube3D(X + 1, Y + 1, Z + 1)),
            new Face3D(new Cube3D(X + 1, Y, Z), new Cube3D(X + 1, Y + 1, Z + 1))
        };
    }
}