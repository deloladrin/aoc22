#!/bin/sh

day=$(date +%d)

folder="AOC22.Day$day"
mkdir "$folder"

echo '<Project Sdk="Microsoft.NET.Sdk">
      
  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net6.0</TargetFramework>
    <Nullable>enable</Nullable>
  </PropertyGroup>
          
  <ItemGroup>
    <None Update="Input.txt">
      <CopyToOutputDirectory>Always</CopyToOutputDirectory>
    </None>
  </ItemGroup>
      
</Project>' > "$folder/$folder.csproj"

echo 'using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace '"$folder"';
      
public class Program
{
    private const string InputFile = "Input.txt";
          
    private static void Task1()
    {
        Console.WriteLine($"Task1: {null}");
    }
      
    private static void Task2()
    {
        Console.WriteLine($"Task2: {null}");
    }
          
    private static void Main(string[] args)
    {
        Task1();
        Task2();
    }
}' > "$folder/Program.cs"

echo '' > "$folder/Input.txt"
