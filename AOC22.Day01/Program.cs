﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AOC22.Day01;

public class Program
{
    private const string InputFile = "Input.txt";
    
    private static void Task1(List<int> elves)
    {
        Console.WriteLine($"Task1: {elves.Max()}");
    }

    private static void Task2(List<int> elves)
    {
        if (elves.Count < 3)
        {
            Console.WriteLine($"Task2: Not enough elves!");
            return;
        }

        elves = elves.OrderByDescending(x => x).ToList();
        Console.WriteLine($"Task2: {elves[0] + elves[1] + elves[2]}");
    }
    
    private static void Main(string[] args)
    {
        List<int> elves = new List<int>();
        int current = 0;

        foreach (string line in File.ReadAllLines(InputFile))
        {
            if (int.TryParse(line, out int value))
            {
                current += value;
                continue;
            }
            
            elves.Add(current);
            current = 0;
        }
        
        Task1(elves);
        Task2(elves);
    }
}