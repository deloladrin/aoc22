using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day02;
      
public class Program
{
    private const string InputFile = "Input.txt";

    private static Result PlayForResult(Shape player, Shape opponent)
    {
        if (player == Shape.Rock)
        {
            if (opponent == Shape.Rock) return Result.Draw;
            if (opponent == Shape.Paper) return Result.Loss;
            if (opponent == Shape.Scissors) return Result.Win;
        }
            
        if (player == Shape.Paper)
        {
            if (opponent == Shape.Rock) return Result.Win;
            if (opponent == Shape.Paper) return Result.Draw;
            if (opponent == Shape.Scissors) return Result.Loss;
        }
            
        if (player == Shape.Scissors)
        {
            if (opponent == Shape.Rock) return Result.Loss;
            if (opponent == Shape.Paper) return Result.Win;
            if (opponent == Shape.Scissors) return Result.Draw;
        }

        return 0;
    }
    
    private static void Task1(List<(char player, char opponent)> games)
    {
        int total = 0;

        foreach ((char player, char opponent) in games)
        {
            Shape p = (Shape) (player - 'X' + 1);
            Shape o = (Shape) (opponent - 'A' + 1);
            Result r = PlayForResult(p, o);

            total += (int) p + (int) r * 3;
        }
        
        Console.WriteLine($"Task1: {total}");
    }

    private static Shape PlayForShape(Shape opponent, Result result)
    {
        if (result == Result.Loss)
        {
            if (opponent == Shape.Rock) return Shape.Scissors;
            if (opponent == Shape.Paper) return Shape.Rock;
            if (opponent == Shape.Scissors) return Shape.Paper;
        }

        if (result == Result.Draw)
        {
            if (opponent == Shape.Rock) return Shape.Rock;
            if (opponent == Shape.Paper) return Shape.Paper;
            if (opponent == Shape.Scissors) return Shape.Scissors;
        }

        if (result == Result.Win)
        {
            if (opponent == Shape.Rock) return Shape.Paper;
            if (opponent == Shape.Paper) return Shape.Scissors;
            if (opponent == Shape.Scissors) return Shape.Rock;
        }

        return 0;
    }
      
    private static void Task2(List<(char player, char opponent)> games)
    {
        int total = 0;

        foreach ((char player, char opponent) in games)
        {
            Shape o = (Shape) (opponent - 'A' + 1);
            Result r = (Result) (player - 'X');
            Shape p = PlayForShape(o, r);
            
            total += (int) p + (int) r * 3;
        }
        
        Console.WriteLine($"Task2: {total}");
    }

    private static void Main(string[] args)
    {
        List<(char player, char opponent)> games = new List<(char player, char opponent)>();

        foreach (string line in File.ReadAllLines(InputFile))
            games.Add((line[2], line[0]));
        
        Task1(games);
        Task2(games);
    }
}
