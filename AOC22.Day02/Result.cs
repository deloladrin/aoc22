namespace AOC22.Day02;

public enum Result
{
    Loss = 0,
    Draw = 1,
    Win = 2
}