using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
      
namespace AOC22.Day10;

public class Program
{
    private const string InputFile = "Input.txt";

    private static void Execute(List<Instruction> instructions, InstructionCallback callback)
    {
        int register = 1;
        int cycle = 1;
        
        foreach (Instruction instruction in instructions)
        {
            switch (instruction.Type)
            {
                case InstructionType.ADDX:
                    callback(cycle++, register);
                    callback(cycle++, register);
                    register += instruction.Value;
                    break;
                
                case InstructionType.NOOP:
                    callback(cycle++, register);
                    break;
            }
        }
    }
    
    private static void Task1(List<Instruction> instructions)
    {
        int total = 0;
        
        Execute(instructions, (cycle, register) =>
        {
            if (cycle == 20 || cycle == 60 || cycle == 100 ||
                cycle == 140 || cycle == 180 || cycle == 220)
            {
                total += cycle * register;
            }
        });
        
        Console.WriteLine($"Task1: {total}");
    }

    private static string Print(bool[,] screen, int offset)
    {
        string output = "";
        
        for (int y = 0; y < screen.GetLength(1); y++)
        {
            if (y != 0)
                output += new string(' ', offset);
            
            for (int x = 0; x < screen.GetLength(0); x++)
                output += screen[x, y] ? '█' : '░';

            if (y != screen.GetLength(1) - 1)
                output += '\n';
        }

        return output;
    }
    
    private static void Task2(List<Instruction> instructions)
    {
        bool[,] screen = new bool[40, 6];

        Execute(instructions, (cycle, register) =>
        {
            int x = (cycle - 1) % screen.GetLength(0);
            int y = (cycle - 1) / screen.GetLength(0);

            if (x >= register - 1 && x <= register + 1)
                screen[x, y] = true;
        });
        
        Console.WriteLine($"Task2: {Print(screen, 7)}");
    }
          
    private static void Main(string[] args)
    {
        List<Instruction> instructions = new List<Instruction>();

        foreach (string line in File.ReadAllLines(InputFile))
        {
            Instruction instruction = new Instruction(line);
            instructions.Add(instruction);
        }
        
        Task1(instructions);
        Task2(instructions);
    }
}
