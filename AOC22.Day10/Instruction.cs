using System;

namespace AOC22.Day10;

public delegate void InstructionCallback(int cycle, int register);

public record Instruction(InstructionType Type, int Value)
{
    public Instruction(string contents) :
        this(0, 0)
    {
        string[] parts = contents.Split(' ');
        Type = Enum.Parse<InstructionType>(parts[0].ToUpper());

        if (parts.Length > 1)
            Value = int.Parse(parts[1]);
    }
}