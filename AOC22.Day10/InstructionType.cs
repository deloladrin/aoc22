using System;

namespace AOC22.Day10;

public enum InstructionType
{
    ADDX,
    NOOP
}